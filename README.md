# kuo

Kubernetes User Operator

## Why do we need users?

Kubernetes doesn't really have a concept of users, only
service accounts. It assumes all apps deployed are handling their own
authentication and authorization. There needs to be some mechanism for
segregating apps and data between different users of the system. 

If we're using Kubernetes as an operating system for the cloud computer, users
need to be able to just log in and use apps like they normally would.
Authenticating from app to app is annoying and breaks the experience.
